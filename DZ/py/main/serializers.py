from django.contrib.auth.models import User
from rest_framework import serializers
from main.models import *


class SortSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Sort
        fields = ('id', 'name', 'flavor', 'descr', 'img')


class IngrSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ingr
        fields = ('id', 'name')