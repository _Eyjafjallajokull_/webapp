from django.shortcuts import render
from main.models import *
from django.contrib.auth.models import User
from rest_framework import viewsets
from main.serializers import *


class SortViewSet(viewsets.ModelViewSet):
    serializer_class = SortSerializer

    def get_queryset(self):
        queryset = Sort.objects.all()
        sort = self.request.query_params.get('s', None)
        if sort is not None:
            queryset = Sort.objects.filter(flavor__in=Ingr.objects.filter(name=sort))
        return queryset


class IngrViewSet(viewsets.ModelViewSet):
    serializer_class = IngrSerializer

    def get_queryset(self):
        queryset = Ingr.objects.all()
        name = self.request.query_params.get('n', None)
        if name is not None:
            queryset = Ingr.objects.filter(sort__id__exact=name)
        return queryset


def index(request):
    return render(request, 'main/index.html')


def sort(request):
    a = list(Sort.objects.filter(flavor__in=Ingr.objects.filter(name=request.POST.get("t", ""))))
    return render(request, 'main/sort.html', {"list": a})
