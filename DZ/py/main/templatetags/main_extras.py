from django import template

from main.models import Ingr, Sort

register = template.Library()


@register.simple_tag
def getflavor(name):
    set = list(Ingr.objects.filter(sort__name__exact=name))
    out = ''
    for item in set:
        out += item.name
        out += ', '
    out = out[:-2]
    return out
