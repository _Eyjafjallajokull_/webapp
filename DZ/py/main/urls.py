from django.conf.urls import url, include
from django.urls import path
from django.views.generic import ListView
from main.models import *
from . import views
from rest_framework import routers


#router = routers.DefaultRouter()
#router.register(r'sort', views.SortViewSet, basename='sort')
#router.register(r'sort?s=', views.SortViewSet, basename='sort')
#router.register(r'ingr', views.IngrViewSet, basename='ingr')
#router.register(r'ingr?n=', views.IngrViewSet, basename='ingr')

urlpatterns = [
    path('', ListView.as_view(queryset=Ingr.objects.all().order_by("name"), template_name="main/index.html")),
    path('sort', views.sort),
    #url(r'^', include(router.urls)),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
