from django.db import models

# Create your models here.


class Ingr(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name


class Sort(models.Model):
    name = models.CharField(max_length=120)
    flavor = models.ManyToManyField(Ingr)
    descr = models.TextField()
    img = models.TextField()

    def __str__(self):
        return self.name
