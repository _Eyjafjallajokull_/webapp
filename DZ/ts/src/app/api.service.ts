import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseurl = "http://127.0.0.1:8000";
  httpHeaders = new HttpHeaders({'Content-type': 'application/json'});

  constructor(private http: HttpClient) { }

  getAllIngr(): Observable<any>{
    return this.http.get(this.baseurl + '/ingr/', {headers: this.httpHeaders});
  }

  getIngr(name): Observable<any>{
    return this.http.get(decodeURI( this.baseurl + '/sort/?s=' + name), {headers: this.httpHeaders });
  }

  getFlav(id): Observable<any>{
    return this.http.get(this.baseurl + '/ingr/?n=' + id, {headers: this.httpHeaders});
  }
}
