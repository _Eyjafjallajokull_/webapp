import {Component, NgZone} from '@angular/core';
import {ApiService} from './api.service';
import {eventTargetLegacyPatch} from 'zone.js/lib/browser/event-target-legacy';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ApiService]
})

export class AppComponent {

  constructor(private api: ApiService) {
    this.getIngr();
  }
  title = 'Ice cream';
  ingr = [{name: 'test'}];
  showTable = true;
  showDescr = false;
  flavor: string;
  obj: any;
  temp = [{name: ''}];

  getIngr = () => {
    this.api.getAllIngr().subscribe(data => {this.ingr = data; }, error => {console.log(error); });
  }

  ingrClick = (ingr) => {
    this.api.getIngr(ingr.name).subscribe(data => {this.obj = data; }, error => {console.log(error); });
    this.showTable = ! this.showTable;
    this.showDescr = ! this.showDescr;
  }


  getFlav = (item) => {
    this.api.getFlav(item.id).subscribe(data => {this.temp = data; } );
  }

  backClick = () => {
      this.api.getAllIngr().subscribe(data => {this.ingr = data; }, error => {console.log(error); });
      this.showTable = ! this.showTable;
      this.showDescr = ! this.showDescr;
  }

  sum = (elem) => {
    this.flavor += elem.name + ' ';

  }
}
