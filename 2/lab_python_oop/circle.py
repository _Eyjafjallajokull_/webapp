from lab_python_oop.geomfig import Figure
from lab_python_oop.colorfig import Color
import math


class Circle(Figure):
    _name = "Circle"

    def __init__(self, radius, color):
        super().__init__(self._name)
        self._radius = radius
        self._color = Color
        self._color.value = color

    def area(self):
        return math.pi * self._radius ** 2

    def __repr__(self):
        return '{0}: \nRadius = {1} \nColor = {2} \nArea = {3}\n'.format(
            self._name,
            self._radius,
            self._color.value,
            self.area()
        )
