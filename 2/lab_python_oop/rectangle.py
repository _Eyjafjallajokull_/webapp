from lab_python_oop.geomfig import Figure
from lab_python_oop.colorfig import Color


class Rectangle(Figure):
    _name = "Rectangle"

    def __init__(self, width, height, color):
        super().__init__(self._name)
        self._width = width
        self._height = height
        self._color = Color
        self._color.value = color

    def area(self):
        return self._width * self._height

    def __repr__(self):
        return '{0}: \nWidth = {1} \nHeight = {2} \nColor = {3} \nArea = {4}\n'.format(
            self._name,
            self._width,
            self._height,
            self._color.value,
            self.area()
        )
