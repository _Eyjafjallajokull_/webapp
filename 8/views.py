from django.shortcuts import render

# Create your views here.


def index(request):
    return render(request, 'main/index.html')


def sort(request):

    if request.POST.get("tsort", "") == "mint":
        return render(request, 'main/sort.html', {'name': 'Mint'})
    elif request.POST.get("tsort", "") == "lemon":
        return render(request, 'main/sort.html', {'name': 'Lemon'})
    elif request.POST.get("tsort", "") == "strawberry":
        return render(request, 'main/sort.html', {'name': 'Strawberry'})


