from lab_python_fp.iterator import Unique
from lab_python_fp.gen import gen_random


data1 = [1, 1, 1, 1, 1, 2, 2, 2, 2, 2]
data2 = gen_random(10, 1, 3)
data3 = ['a', 'A', 'b', 'B']

# Реализация задания 2
print(list(Unique(data1)))
print(list(Unique(data2)))
print(list(Unique(data3)))
print(list(Unique(data3, ignore_case=True)))