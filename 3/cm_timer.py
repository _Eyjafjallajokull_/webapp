from lab_python_fp.mtimer import Timer, Timer2
from time import sleep


with Timer():
    sleep(5.5)

with Timer2():
    sleep(5.5)