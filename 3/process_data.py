import requests
from lab_python_fp.gen import gen_random
from lab_python_fp.decorator import print_result
from lab_python_fp.mtimer import Timer, Timer2
from lab_python_fp.iterator import Unique
from lab_python_fp.gen import field

response = requests.get('https://raw.githubusercontent.com/iu5team/iu5web-fall-2020/master/labs/lab3/data/data_light.json')


@print_result
def f1(arg):
    return list(Unique(list(field(arg, "job-name")), ignore_case=True))


@print_result
def f2(arg):
    return list(filter(lambda x: x.startswith('Программист'), arg))


@print_result
def f3(arg):
    return list(map(lambda x: x + ' с опытом Python', arg))


@print_result
def f4(arg):
    pairs = zip(arg, gen_random(len(arg), 100000, 200000))
    return ['{}, зарплата {} руб.'.format(*i) for i in pairs]


with Timer2():
    f4(f3(f2(f1(response.json()))))