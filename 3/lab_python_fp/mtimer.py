import time
from contextlib import contextmanager
from datetime import datetime


class Timer:
    def __enter__(self):
        self.start = time.time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        ti = (time.time()) - self.start
        print('time:', ti)

@contextmanager
def Timer2():
    start_time = datetime.now()
    yield
    end_time = datetime.now()
    print('time:', end_time - start_time)