def print_result(func):
    def dec_func(*args, **kwargs):
        print(func.__name__)
        res = func(*args, **kwargs)
        if type(res) == dict:
            for key in res:
                print(key, '=', res[key])
        elif type(res) == list:
            for item in res:
                print(item)
        else:
            print(res)
        return res

    return dec_func
