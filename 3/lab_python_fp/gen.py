import random


def field(items, *args):
    if len(args) == 1:
        for item in items:
            yield item[args[0]]
    else:
        for item in items:
            fields_item = {}
            for fld in args:
                if fld in item:
                    fields_item.update({fld: item[fld]})
            yield fields_item


def gen_random(num_count, begin, end):
    for i in range(num_count):
        yield random.randint(begin, end)
